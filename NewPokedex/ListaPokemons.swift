//
//  ListaPokemons.swift
//  NewPokedex
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class ListaPokemons: UIViewController, UITableViewDataSource {

    
    @IBOutlet weak var lstPokemon: UITableView!
    
    var lista_nome_pokemon: [String] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        lstPokemon.dataSource = self
        lista_nome_pokemon.append("Bulbassauro")
        lista_nome_pokemon.append("Ivyssauro")
        lista_nome_pokemon.append("Venussauro")
        lista_nome_pokemon.append("Charmander")
        lista_nome_pokemon.append("Charmeleon")
        lista_nome_pokemon.append("Charizard")
        lista_nome_pokemon.append("Squirtle")
        lista_nome_pokemon.append("Wartortle")
        lista_nome_pokemon.append("Blastoise")
        lista_nome_pokemon.append("Pikachu")

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista_nome_pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "celulaPokemon", for: indexPath) as? TableViewCell {
            cell.txt_titulo.text = lista_nome_pokemon[indexPath.row]
            
            if indexPath.row >= 3 && indexPath.row <= 6 {
            }
            return cell
        }
        return TableViewCell()
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
