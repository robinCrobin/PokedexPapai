//
//  TableViewCell.swift
//  NewPokedex
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var txt_titulo: UILabel!
    @IBAction func txt_propriedade1(_ sender: Any) {
    }
    
    @IBAction func txt_propriedade2(_ sender: Any) {
    }
    
    @IBOutlet weak var img_pokemon: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
